// call dependensies
var React = require('react');
var {Link, IndexLink} = require('react-router');

var Navigation = () =>{
	return (
		<div className="top-bar">
			<div className="top-bar-left">
				<ul className="menu">
					<li className="menu-text">
						React Timer App
					</li>
					<li className="menu-text">
						<IndexLink to="/" activeClaseName="active-link">Timer</IndexLink>
					</li>
					<li className="menu-text">
						<IndexLink to="/countdown" activeClaseName="active-link">Counter</IndexLink>
					</li>
				</ul>
			</div>
			<div className="top-bar-right">
				<ul className="menu">
					<li className="menu-text">
						Ceated by Daniel
					</li>
				</ul>
			</div>
		</div>
	);
};

module.exports = Navigation;